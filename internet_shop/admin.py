# -*- coding: utf-8 -*-
from django.contrib import admin

from internet_shop.models import Category, Product


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)


class ProductAdmin(admin.ModelAdmin):
    list_display = ('title',
                    'image',
                    'description',
                    'quantity',
                    'price')


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
