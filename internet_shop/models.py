# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings


class Category(models.Model):

    title = models.CharField(
        verbose_name='название категории товара',
        max_length=200)

    class Meta:
        verbose_name = 'категория'
        verbose_name_plural = 'категории'

    def __str__(self):
        return self.title


class Product(models.Model):

    category = models.ForeignKey(
        Category,
        related_name='product',
        verbose_name='категория')
    title = models.CharField(
        verbose_name='название товара',
        max_length=300)
    image = models.ImageField(
        verbose_name='изображение товара',
        upload_to='product_image')
    description = models.TextField(
        verbose_name='описание товара')
    price = models.IntegerField(
        verbose_name='цена')
    quantity = models.IntegerField(
        verbose_name='количество товара на складе')

    class Meta:
        verbose_name = 'товар'
        verbose_name_plural = 'товары'

    def __str__(self):
        return self.title


class SelectedProductUser(models.Model):

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name='пользователь')
    product = models.ForeignKey(
        Product,
        related_name='selected_products_user',
        verbose_name='товар')

    class Meta:
        verbose_name = 'товар в корзине'
        verbose_name_plural = 'товары в корзине'

    def __str__(self):
        return str(self.product)
