from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

from internet_shop.views import (ProductListView, ProductDetailsView,
                                 BasketView, AddProductToBasketView,
                                 VKAuthView, ElasticSearchView)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', ProductListView.as_view(), name='product_list'),
    url(r'^product_details/(?P<product_id>\d+)$', ProductDetailsView.as_view(), name='product_details'),
    url(r'^add_product_in_basket/(?P<product_id>\d+)$', AddProductToBasketView.as_view(), name='add_product_in_basket'),
    url(r'^basket/$', BasketView.as_view(), name='basket'),
    url(r'^vkauth/$', VKAuthView.as_view(), name='vkauth'),
    url(r'^search/$', TemplateView.as_view(template_name='internet_shop/search.html'), name='search'),
    url(r'^get_search/$', ElasticSearchView.as_view(), name='get_search')
]

urlpatterns.extend(static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT))
urlpatterns.extend(static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT))
