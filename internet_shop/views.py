from datetime import datetime

import vk
from elasticsearch import Elasticsearch
from django_robokassa.forms import RobokassaForm

from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.views import generic
from django.db.models import Sum

from internet_shop.models import Product, SelectedProductUser


class ProductListView(generic.ListView):
    queryset = Product.objects.all()


class ProductDetailsView(generic.DetailView):
    model = Product
    pk_url_kwarg = 'product_id'


class AddProductToBasketView(generic.View):

    def post(self, request, product_id):
        product = get_object_or_404(Product, id=product_id)
        product.selected_products_user.create(user=request.user)
        return redirect('basket')


class BasketView(generic.TemplateView):
    template_name = 'internet_shop/basket.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        selected_products = SelectedProductUser.objects.filter(
            user_id=self.request.user)
        price_sum = selected_products.aggregate(
            sum=Sum('product__price'))['sum']
        ctx['products'] = selected_products
        product_title_list = list(selected_products.values_list(
            'product__title', flat=True))
        ctx['price_sum'] = price_sum
        ctx['robokassa_form'] = RobokassaForm(initial={
            'OutSum': price_sum,
            'Desc': product_title_list,
            'Email': self.request.user.email,
            'Culture': 'ru'
        })
        return ctx

'''НИЖЕ НЕ СМОТРИ!!!'''
class VKAuthView(generic.View):

    def get(self, *args, **kwargs):
        session = vk.Session()
        api = vk.API(session, v='5.64', lang='ru', timeout=10)
        api.account.getProfileInfo()

        return redirect('/')


class ElasticSearchView(generic.View):

    def get(self, *args, **kwargs):
        es = Elasticsearch()
        products = Product.objects.all()
        id = 0
        for product in products:
            id += 1
            doc = {
                'title': product.title,
                'description': product.description,
                'timestamp': datetime.now(),
            }
            es.index(index="test-index1", doc_type='tweet', id=id,
                           body=doc)
            es.indices.refresh(index="test-index1")
        res = es.search(index="test-index1", body={"query": {"match_all": {}}})
        for hit in res['hits']['hits']:
            print((hit["_source"]).get('title'))
        return redirect('/')
